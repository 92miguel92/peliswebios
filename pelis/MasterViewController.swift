//
//  MasterViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, ConnectionDelegate, PelisTableDelegate{
    
    internal func saved(peli: Pelicula) {
        self.pelis.append(peli)
        self.tableView.reloadData()
    }

    var detailViewController: DetailViewController? = nil
    var pelis = [Pelicula]()
    let login = "test123", password = "test123"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createPelis()

        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
 
    }
    
    func insertNewObject(_ sender: Any) {
        let pelisAdd = PelisTableViewController()
        pelisAdd.delegate = self
        self.navigationController?.pushViewController(pelisAdd, animated: true)
    }
    
    func createPelis() {
        // Creamos la conexión
        let connection = Connection(delegate: self)
        // Lanzamos una petición
        connection.startConnection(URLSession.shared, with: "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog")
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        if connection.name == "delete" {
            // Gestionar borrado
        }
        else{
            do{
                if let jsonPeliculas = try JSONSerialization.jsonObject(with: data, options: []) as? [Any]{
                    
                    for pelicula in jsonPeliculas{
                        self.pelis.append(Pelicula(json: pelicula as! [String : String])!)
                    }
                }
            }catch{
                print (error.localizedDescription)
            }
        }

        self.tableView.reloadData()
    }
    
    func connectionFailed(_ connection: Connection, with error: String){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        print(error)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
/*
    func insertNewObject(_ sender: Any) {
        objects.insert(NSDate(), at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
*/
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = pelis[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pelis.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let object = self.pelis[indexPath.row]
        let urlString = object.urlPoster
        
        cell.textLabel!.text = object.title
        cell.imageView?.contentMode = .scaleAspectFit
        if(cell.imageView?.image == nil){
            cell.imageView?.image = UIImage(named: "Placeholder.png")
        }
        
        if(self.pelis[indexPath.row].image == nil){
            if let url = URL(string:urlString!) {
                session.dataTask( with:url, completionHandler: { data, response, error in
                    if error == nil, let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                        DispatchQueue.main.async {
                            let image = UIImage(data: data)
                            cell.imageView?.image = image
                            self.pelis[indexPath.row].image = image
                            print ("Downloaded \(object.title)")
                        }
                    }
                }).resume()
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let connectionDelete = Connection(name : "delete", delegate : self)
            connectionDelete.startConnection(URLSession.shared, with: "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(self.pelis[indexPath.row].identif!)", self.login, self.password, "".data(using: .utf8)!)
            self.pelis.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
}

