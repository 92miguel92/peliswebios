//
//  Pelis.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class Pelicula
{
    var identif : Int?
    var title : String?
    var year : Int?
    var director : String?
    var urlPoster : String?
    var rented : Bool?
    var synopsis : String?
    var image : UIImage?
    
    init?(json: [String : String]){
        guard let identif = json["id"],
        let title = json["title"],
        let year = json["year"],
        let director = json["director"],
        let urlPoster = json["poster"],
        let rented = json["rented"],
        let synopsis = json["synopsis"]
        else{
            return nil
        }
        
        self.identif = Int(identif)
        self.title = title
        self.year = Int(year)
        self.director  = director
        self.urlPoster = urlPoster
        if(rented == "1"){
            self.rented = true
        }else{
            self.rented = false
        }
        self.synopsis = synopsis
    }
    
    /*init(title: String, year: Int, director: String, urlPoster: String, rented: Bool, synopsis: String)
    {
        self.title = title
        self.year = year
        self.director  = director
        self.urlPoster = urlPoster
        self.rented = rented
        self.synopsis = synopsis
    }*/
    init?() {
        // No hacer nada
    }
}


