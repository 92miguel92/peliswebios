//
//  Connection.swift
//  pelis
//
//  Created by Master Móviles on 04/02/2017.
//  Copyright © 2017 Antonio Pertusa. All rights reserved.
//

import Foundation

protocol ConnectionDelegate {
    func connectionSucceed(_ connection : Connection, with data: Data)
    func connectionFailed(_ connection: Connection, with error: String)
}

class Connection {
    
    var delegate:ConnectionDelegate?
    var name : String?
    
    init(delegate : ConnectionDelegate) {
        self.delegate = delegate
    }
    
    init(name:String, delegate:ConnectionDelegate) {
        self.delegate = delegate
        self.name = name
    }
    
    func startConnection(_ session: URLSession, with urlString:String)
    {
        if let encodedString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: encodedString) {
                self.startConnection(session, with: url)
            }
        }
    }
    
    func startConnection(_ session: URLSession, with url:URL)
    {
        self.startConnection(session, with: URLRequest(url:url))
    }
    
    func startConnection(_ session: URLSession, with request:URLRequest)
    {
        session.dataTask(with: request, completionHandler: { data, response, error in
            
            if let error = error {
                self.delegate?.connectionFailed(self, with: error.localizedDescription)
            }
            else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.delegate?.connectionSucceed(self, with: data!)
                    }
                }
                else {
                    let errorMessage=("Received status code: \(res.statusCode)")
                    self.delegate?.connectionFailed(self, with: errorMessage)
                }
            }
        }).resume()
    }
    
    //conexión con user
    func startConnection(_ session: URLSession, with urlString:String,_ login:String,_ password:String,
                         _ body:Data)
    {
        if let encodedString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: encodedString) {
                self.startConnection(session, with: url, login, password, body)
            }
        }
    }
    
    func startConnection(_ session: URLSession, with url:URL,_ login:String,_ password:String,_ body:Data)
    {
        var request = URLRequest(url: url)
        if self.name == "alquilar" || self.name == "devolver" || self.name == "editar"{
            request.httpMethod = "PUT"
        }else if self.name == "crear"{
            request.httpMethod = "POST"
            request.httpBody = body
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }else if self.name == "delete"{
            request.httpMethod = "DELETE"
        }else{
            request.httpMethod = "GET"
        }
        self.startConnection(session,with: request, login, password)
    }
    
    func startConnection(_ session:URLSession, with request:URLRequest,_ login:String,_ password:String)
    {
        let authStr = login + ":" + password
        if let authData = authStr.data(using: .utf8) {
            let authValue = "Basic \(authData.base64EncodedString(options: []))"
            let config = URLSessionConfiguration.default
            config.httpAdditionalHeaders = ["Accept" : "application/json",
                                            "Authorization" : authValue]
            let session = URLSession(configuration: config)
            
            session.dataTask(with: request, completionHandler: { data, response, error in
                
                if let error = error {
                    self.delegate?.connectionFailed(self, with: error.localizedDescription)
                }
                else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 200 {
                        DispatchQueue.main.async {
                            self.delegate?.connectionSucceed(self, with: data!)
                        }
                    }
                    else {
                        let errorMessage=("Received status code: \(res.statusCode)")
                        self.delegate?.connectionFailed(self, with: errorMessage)
                    }
                }
            }).resume()
        }
    }
    
}


