//
//  PelisTableViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 19/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

protocol PelisEditTableDelegate {
    func edit(peli : Pelicula)
}

class PelisEditTableViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate, ConnectionDelegate {
    
    var peli = Pelicula()!
    var delegate:PelisEditTableDelegate?
    let login = "test123", password = "test123"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Save button
        let saveButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(edit))
        self.navigationItem.rightBarButtonItem = saveButton
        
        self.navigationItem.title = self.peli.title
    }
    
    func getCellTextField(section : Int) -> String?
    {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: section))
        let textField = cell?.subviews[1] as? UITextField
        return textField?.text
    }
    
    func getCellTextView(section : Int) -> String?
    {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: section))
        let textView = cell?.subviews[1] as? UITextView
        return textView?.text
    }
    
    func edit() {
        
        var dict: [String : String] = ["title":"","year":"","director":"","poster":"","rented":"0","synopsis":""]
        
        if let title = self.getCellTextField(section: 0) {
            self.peli.title = title
            dict["title"] = title
        }
        if let year = self.getCellTextField(section: 1) {
            self.peli.year = Int(year)
            dict["year"] = year
        }
        if let director = self.getCellTextField(section: 2) {
            self.peli.director = director
            dict["director"] = director
        }
        if let poster = self.getCellTextField(section: 3) {
            self.peli.urlPoster = poster
            dict["poster"] = poster
        }
        if let synopsis = self.getCellTextView(section: 4) {
            self.peli.synopsis = synopsis
            dict["synopsis"] = synopsis
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            if let str = String(data: data, encoding: .utf8) { // Para imprimirlo por pantalla
                print (str)
                let connection = Connection(name : "editar",delegate: self)
                // Lanzamos una petición
                connection.startConnection(URLSession.shared, with: "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(self.peli.identif)", self.login, self.password,data)
            }
            
        } catch {
            print (error.localizedDescription)
        }
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:AnyObject]
            // Hacer algo con la variable json
            self.peli.identif = json["id"] as! Int?
        } catch {
            print (error.localizedDescription)
        }
        
        self.delegate?.edit(peli: self.peli)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func connectionFailed(_ connection: Connection, with error: String) {
        print(error)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch(section) {
        case 0: return "Title"
        case 1: return "Year"
        case 2: return "Director"
        case 3: return "URL poster"
        case 4: return "Synopsis"
        default: return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.accessoryType = .none
        
        if (indexPath.section != 4) {
            let textField = UITextField(frame: CGRect(x:10,y:10,width:self.tableView.frame.size.width-10, height:30))
            
            textField.adjustsFontSizeToFitWidth = true
            textField.delegate = self
            textField.tag = indexPath.section
            textField.isEnabled = true
            
            if indexPath.section==0 {
                textField.text=self.peli.title
            }
            else if indexPath.section == 1 {
                if let year = peli.year {
                    textField.text = "\(year)"
                }
            }
            else if indexPath.section == 2 {
                textField.text=self.peli.director
            }
            else if indexPath.section == 3 {
                textField.text=self.peli.urlPoster
            }
            
            cell.addSubview(textField)
        }
        else {
            let textView = UITextView(frame: CGRect(x:10, y:10, width:self.tableView.frame.size.width-10, height:300))
            textView.font = textView.font?.withSize(15)
            textView.delegate = self
            
            textView.text=self.peli.synopsis
            
            cell.addSubview(textView)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 {
            return 300
        }
        return 40
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0: self.peli.title = textField.text
        case 1: self.peli.year = Int(textField.text!)
        case 2: self.peli.director = textField.text
        case 3: self.peli.urlPoster = textField.text
        default: print("Unknown!") //  Esto no debería pasar nunca, añadido para silenciar el warning
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.peli.synopsis = textView.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
