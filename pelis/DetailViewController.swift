//
//  DetailViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, ConnectionDelegate, PelisEditTableDelegate{
    
    internal func edit(peli: Pelicula) {
        self.detailItem?.title = peli.title
        self.detailItem?.year = peli.year
        self.detailItem?.director = peli.director
        self.detailItem?.synopsis = peli.synopsis
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textButton: UIButton!
    var detailItem: Pelicula?
    let login = "test123", password = "test123"
    @IBAction func alquilerPeli(_ sender: Any) {
        
        if(self.detailItem?.rented)!{
            devolverPeli((self.detailItem?.identif)!)
        }else{
            alquilarPeli((self.detailItem?.identif)!)
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let peli = self.detailItem {

            if let title = peli.title {
                self.navigationItem.title = "\(title)"
                if let year = peli.year {
                    self.navigationItem.title =  "\(title) (\(year))"
                }
            }
            
            self.textView?.text = peli.synopsis
            self.imageView?.image = peli.image
        }
    }

    func alquilarPeli(_ id: Int){
        let connection = Connection(name : "alquilar",delegate: self)
        let stringURL = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(id)/rent"
        connection.startConnection(URLSession.shared, with: stringURL, self.login, self.password,"".data(using: .utf8)!)
    }
        
    func devolverPeli(_ id: Int){
        let connection = Connection(name : "devolver",delegate: self)
        let stringURL = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(id)/return"
        connection.startConnection(URLSession.shared, with: stringURL, self.login, self.password,"".data(using: .utf8)!)
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        if(self.detailItem?.rented)!{
            self.textButton.setTitle("Libre", for: .normal);
        }else{
            self.textButton.setTitle("Alquilada", for: .normal);
        }
    }
    
    func connectionFailed(_ connection: Connection, with error: String){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        print(error)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
        
        // Edit button
        let saveButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editNewObject(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
        
        //asignamos el label del boton alquilar
        if(self.detailItem?.rented)!{
            self.textButton.setTitle("Alquilada", for: .normal);
        }else{
            self.textButton.setTitle("Libre", for: .normal);
        }
    }
    
    func editNewObject(_ sender: Any) {
        let peliEdit = PelisEditTableViewController()
        peliEdit.delegate = self
        self.navigationController?.pushViewController(peliEdit, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async{ // Para hacer que el textView se visualize desde la primera línea
            self.textView.scrollRangeToVisible(NSMakeRange(0, 0))
        }
    }

}

